FROM adoptopenjdk/openjdk11
COPY build/libs/hello-world-0.1.0.jar hello-world.jar
EXPOSE 3000
ENTRYPOINT ["java","-jar","./hello-world.jar"]